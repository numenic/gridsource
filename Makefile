# ================================================================
PKG_NAME := gridsource
# ================================================================
.PHONY: help bump-patch bump-minor bump-major
.DEFAULT_GOAL := help
define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT

define DETECT_VENV
import subprocess as sp, shlex, re
import os
cwd = os.getcwd()
info = sp.check_output(shlex.split("poetry show -v --only=dev"), cwd=cwd).decode()
venv = re.match("Using virtualenv:.*", info).group().split(":")[1].strip()
print(venv)
endef
export DETECT_VENV
VENV_PATH := $(shell python -c '$(DETECT_VENV)')
VENV_NAME := $(shell basename $(VENV_PATH))


define ARE_YOU_SURE
ans = input('are you sure? [y/N]')
if ans.lower() not in ('yes', 'y', 'ok'):
	print('skip...')
	import sys; sys.exit(1)
endef
export ARE_YOU_SURE
#---------------------------------------------------------------

are_you_sure:
	@python -c "$$ARE_YOU_SURE"

help:
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

bump-patch: select_venv ## patch bump "_._.+1"
	@$(VENV_PATH)/bin/bump2version --config-file setup.cfg patch

bump-minor: are_you_sure select_venv ## minor bump "_.+1._"
	@$(VENV_PATH)/bin/bump2version --config-file setup.cfg minor

bump-major: are_you_sure select_venv ## major bump "+1._._"
	@$(VENV_PATH)/bin/bump2version --config-file setup.cfg major

# dist
select_venv:
	@python -c "$$DETECT_VENV"


test-python:  select_venv  ## test python VENV
	@$(VENV_PATH)/bin/python -c "import sys; print(sys.version); print(sys.executable)"
	@echo PKGNAME= $(PKG_NAME)

clean: clean-build clean-pyc clean-test ## remove all build, test, coverage and Python artifacts

clean-build: ## remove build artifacts
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +

clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-test: ## remove test and coverage artifacts
	rm -fr .tox/
	rm -f .coverage
	rm -fr htmlcov/
	rm -fr .pytest_cache

lint: select_venv  ## apply isort and black
	@$(VENV_PATH)/bin/isort .
	@$(VENV_PATH)/bin/black .

build: clean ## builds source and wheel package
	poetry build
	ls -l dist

release: build  ## builds source and upload to private-pypi
	# rsync dist/* numeric@ssh-numeric.alwaysdata.net:~/private_pypi/$(PKG_NAME)/
	poetry publish

# --------------------------------------------------------------------------------
# Poetry installation process
lock:  ## poetry lock
	poetry lock

install:  ## poetry install
	poetry install

lock-install: lock install  ## poetry lock && poetry install

reinstall:
	poetry cache clear . --all
	poetry env remove $(VENV_NAME)
	poetry lock
	poetry install


